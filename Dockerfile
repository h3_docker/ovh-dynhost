FROM alpine:latest

RUN apk -U --no-cache upgrade && \
  apk add --update --no-cache curl bind-tools

WORKDIR /root

COPY dynhost.sh .
RUN chmod +x ./dynhost.sh

RUN ln -sf /root/dynhost.sh /etc/periodic/15min/dynhost

CMD ["crond", "-f"]